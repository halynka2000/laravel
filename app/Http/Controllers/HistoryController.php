<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\saveHistoryRequest;
use App\Models\User;
use Auth;

class HistoryController extends Controller
{
    use ValidatesRequests;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        $histories = $user->histories()->get();

        //отобразить вьюху
        return view('histories', ['histories' => $histories]);
    }

    public function show($id) {
        $user = Auth::user();
        $history= $user->histories()->findOrFail($id);

        return view('histories.show', ['history' => $history]);
    }

    public function create() {
        return view('histories.create');
    }

    public function store(saveHistoryRequest $request) {
        $user = Auth::user();
        //создание записи в Бд взяв все данные с запроса
        $user->histories()->create($request->validated());

        return redirect()->route('histories.index');
    }

    public function edit($id) {
        $user = Auth::user();
        //находим запись по id
        $history= $user->histories()->findOrFail($id);

        //передаем данные записи у вьюху
        return view('histories.edit', ['history' => $history]);
    }

    public function update(saveHistoryRequest $request, $id) {
        $user = Auth::user();
        //находим запись по id
        $history= $user->histories()->findOrFail($id);
        $history->update($request->validated());

        //вернуть на список
        return redirect()->route('histories.index');
    }


    public function destroy($id) {
        $user = Auth::user();
        $history= $user->histories()->findOrFail($id);
        $history->delete($id);

        return redirect()->route('histories.index');
    }
}
