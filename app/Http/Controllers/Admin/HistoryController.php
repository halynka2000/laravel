<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\saveHistoryRequest;
use App\Models\History;
use App\Models\User;

class HistoryController extends Controller
{
    public function index() {
        //получить все данные с таблицы
        $histories = History::orderBy('created_at', 'desc')->get();

        //отобразить вьюху
        return view('admin.histories.index', compact('histories'));
    }

    public function create() {
        $userList = User::all();
        return view('admin.histories.create', compact('userList'));
    }

    public function store(saveHistoryRequest $request) {

        //создание записи в Бд взяв все данные с запроса
        $history = History::create($request->all());

        if($history) {
            return redirect()
                ->back()
                ->withSuccess('History created successfully!');
        } else {
            return back()
                ->withErrors('Save error')
                ->withInput();
        }
    }

    public function edit($id) {
        //находим запись по id
        $history = History::find($id);

        $userList = User::all();
        //передаем данные записи у вьюху
        return view('admin.histories.edit', compact('history', 'userList'));
    }

    public function update(saveHistoryRequest $request, $id) {
        //находим запись по id
        $history = History::find($id);
        if(empty($history)) {
            return back()->withErrors('Record not found')->withInput();
        }
        //заполняем запись данными из формы
        $data = $request->all();
        //сохранить изменения
        $result = $history
            ->update($data);

        if($result) {
            //вернуть на страницу редактирования с сообщением
            return redirect()
                ->back()
                ->withSuccess('History changed successfully!');
        //если не удалось сохранить
        } else {
            return back()
                ->withErrors('Save error')
                ->withInput();
        }
    }

    public function show($id) {
        $history = History::find($id);

        return view('admin.histories.show', compact('history'));
    }

    public function destroy($id) {
        History::find($id)->delete();
        return redirect()->back()->withSuccess('History deleted successfully!');
    }
}
