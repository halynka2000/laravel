<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('histories', App\Http\Controllers\HistoryController::class);


Route::middleware(['role:admin'])->prefix('admin')->group(function () {
    Route::resource('histories', App\Http\Controllers\Admin\HistoryController::class)->names(['index' => 'admin-histories.index',
                                                                                        'create' => 'admin-histories.create',
                                                                                        'store' => 'admin-histories.store',
                                                                                        'show' => 'admin-histories.show',
                                                                                        'edit' => 'admin-histories.edit',
                                                                                        'update' => 'admin-histories.update',
                                                                                        'destroy' => 'admin-histories.destroy']);
});

