@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Histories</h3>
    <a href="{{ route('histories.create')}}" class="btn btn-success">Create</a>
    <div class="row justify-content-center">
        <div class="col-md-10 col-md-offset-1">
            <table class="table">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>TITLE</td>
                        <td>USER</td>
                        <td>ACTION</td>
                    </tr>
                </thead>
                <tbody>

                    @if($histories->count() == 0)
                        <tr><td> Entries not created yet </td></tr>
                    @else
                        @foreach ($histories as $history)
                            <tr>
                                <td>{{$history->id}}</td>
                                <td>{{$history->title}}</td>
                                <td>{{$history->user->name}}</td>
                                <td>
                                    <a href="{{ route('histories.show', $history->id)}}">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="{{ route('histories.edit', $history->id)}}">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['histories.destroy', $history->id], 'id' => 'delete']) !!}
                                        <button onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
