@extends('layouts.app')

@section('content')

@include('errors')
    <div class="container">
        <h3>Create histories</h3>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => ['histories.store']]) !!}
                <div class="form-group">
                    <label for="historyTitle">Title</label>
                    <input type="text" class="form-control" id="historyTitle" name="title" value="{{old('title')}}">
                    <br>
                    <label for="historyDescription">Description</label>
                    <textarea name="description" cols="30" rows="10" class="form-control" id="historyDescription">{{old('description')}}</textarea>
                    <br>
                    <label for="historyUser">User name</label>
                    <select name="user_id" class="form-control">
                        <option value="{{ Auth::user()->id }}" selected>
                            {{ Auth::user()->name }}
                        </option>
                    </select>
                    <br>
                    <button class="btn btn-success">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
