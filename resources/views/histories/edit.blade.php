@extends('layouts.app')

@section('content')

@include('errors')
    <div class="container">
        <h3>Edit history # - {{$history->id}}</h3>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => ['histories.update', $history->id], 'method' => 'PUT']) !!}
                <div class="form-group">
                    <label for="historyTitle">Title</label>
                    <input type="text" class="form-control" name="title" id="historyTitle" value="{{$history->title}}">
                    <br>
                    <label for="historyDescription">Description</label>
                    <textarea name="description" cols="30" rows="10" class="form-control" id="historyDescription">{{$history->description}}</textarea>
                    <br>
                    <label for="historyUser">User name</label>
                    <select name="user_id" class="form-control">
                        <option value="{{ $history->user_id }}" selected>
                            {{ $history->user->name }}
                        </option>
                    </select>
                    <br>
                    <button class="btn btn-success">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
