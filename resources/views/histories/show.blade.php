@extends('layouts.app')

@section('content')

@include('errors')
    <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h3>TITLE: {{$history->title}}</h3>
               <h3>USER NAME: {{$history->user->name}}</h3>
               <p>
                   DESCRIPTION: {{$history->description}}
               </p>
           </div>
       </div>
    </div>

@endsection
