@extends('layouts.admin_layout')

@section('title', 'View history')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>View details</h2>
                    <ul class="header-dropdown dropdown">
                        <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    </ul>
                </div>
                <div class="body">
                    <table id="mainTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>User name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$history->id}}</td>
                                <td>{{$history->title}}</td>
                                <td>{{$history->user->name}}</td>
                                <td>{{$history->description}}</td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
