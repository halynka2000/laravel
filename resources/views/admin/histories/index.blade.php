@extends('layouts.admin_layout')

@section('title', 'Histories')

@section('content')

@include('success')

    <div class="row clearfix">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light page_menu">
                <a class="navbar-brand" href="javascript:void(0);">Histories</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars text-muted"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <div class="ml-auto">
                        <a href="{{ route('admin-histories.create') }}" type="button" class="btn btn-default">Create New</a>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive invoice_list">
                        <table class="table table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 50px">#</th>
                                    <th>Title</th>
                                    <th style="width: 400px">User name</th>
                                    <th style="width: 100px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($histories as $history)


                                    <tr>
                                        <td>
                                            <span>{{$history->id}}</span>
                                        </td>
                                        <td>
                                            {{$history->title}}
                                        </td>
                                        <td>
                                            <div class="ml-3">
                                                <p>{{$history->user->name}}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-default"  href="{{ route('admin-histories.show', $history->id)}}" title="View"><i class="icon-eye"></i></a>
                                            <a href="{{ route('admin-histories.edit', $history->id) }}" class="btn btn-sm btn-default "title="Edit"><i class="icon-pencil"></i></a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin-histories.destroy', $history->id], 'id' => 'admin-delete']) !!}
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-default" title="Delete"><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
