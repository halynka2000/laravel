@extends('layouts.admin_layout')

@section('title', 'Edit history')

@section('content')

    <div class="block-header">
        <div class="row clearfix">
            <div class="col-xl-5 col-md-5 col-sm-12">
                <h1>Hi, Welcomeback!</h1>
                <span>JustDo @yield('title'),</span>
            </div>
        </div>

        @include('success')
        @include('errors')
    </div>

    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Edit history #{{$history->id}}</h2>
                </div>
                <div class="body">
                    {!! Form::open(['route' => ['admin-histories.update', $history->id], 'method' => 'PUT']) !!}
                        <div class="form-group c_form_group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title" value="{{$history->title}}" required>
                        </div>
                        <div class="form-group c_form_group">
                            <label>User name</label>
                            <select name="user_id" class="form-control" required>
                                @foreach ($userList as $userOption)
                                    <option value="{{ $userOption->id}}"
                                        @if ($userOption->id == $history->user->id) selected @endif >
                                        {{$userOption->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group c_form_group">
                            <label>Description</label>
                            <textarea class="form-control" rows="5" cols="30" name="description">{{ old('description', $history->description)}}</textarea>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary theme-bg">Change</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
