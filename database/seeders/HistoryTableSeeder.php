<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $history = [];

        for($i = 0; $i <= 10; $i++) {
            $hName = "Посещение #".$i;
            $history[] = [
                'title' => $hName,
                'user_id' => $i+1,
            ];
        }

        \DB::table('histories')->insert($history);
    }
}
