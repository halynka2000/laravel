<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userName = [
            'lina',
            'taras',
            'petro',
            'andriy',
            'ostap',
            'roman',
            'anna',
            'tonya',
            'kira',
            'valera',
            'olga'
        ];

        $data = [];


        for($i = 0; $i <= 10; $i++) {
            $data[] = [
                'name' => $userName[$i],
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('password'),
            ];
        }

        \DB::table('users')->insert($data);
    }
}
